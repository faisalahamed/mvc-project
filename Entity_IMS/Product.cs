﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_IMS
{
    public class Product
    {
        public int ProductId { get; set; }
        [Required, MaxLength(255)]
        public string ProductName { get; set; }
        public float OriginalPrice { get; set; }
        public float SellingPrice { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
