﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Entity_IMS
{
    public class User
    {

        public int UserId { get; set; }
        [Required, MaxLength(255)]
        public string FullName { get; set; }
        [Required, MaxLength(255), Index(IsUnique = true)]
        public string UserName { get; set; }
        [Required, MaxLength(255)]
        public string Password { get; set; }
        [MaxLength(255)]
        public string Email { get; set; }
        [MaxLength(12)]
        public string Phone { get; set; }
        public int UserType { get; set; }
    }
}
