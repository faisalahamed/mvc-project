﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_IMS
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public List<Product> Products { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public DateTime? OrderDate { get; set; }

    }
}
