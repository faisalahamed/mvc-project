﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_IMS
{
   public class Employee:User
    {
        [Required]
        public double Salary { get; set; }
    }
}
